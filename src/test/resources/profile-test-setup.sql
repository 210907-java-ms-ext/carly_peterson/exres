drop table if exists reimbursement, profile;
create table profile (
	e_id int primary key,
	username varchar(50) unique not null,
	pass_key varchar(772) not null,
	access_level varchar(8) not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null
);
insert into profile (e_id, username, pass_key, access_level, first_name, last_name) values
    (1, 'user', 'password', 'EMPLOYEE', 'Alexa', 'Prince'),
    (2, 'user1', 'password', 'EMPLOYEE', 'Ciri', 'Schneewitchen'),
    (3, 'user3', 'QMhsGjgRezMUv94r5XrBOt3s6tCf8qIosp7+tvRRXpQJwFkvoVouZWgftYSSaJXNomEW6WI3Taqf0es6uqEWTA==Zh8O/6MqKjWu5Nlmw3AFsFCm58E7inDkQOnoQ2cZ39Quakn9fP5NGv6RB/N1l2swkBhm70r31ZePMisnII3VqYvit40ANKpxPgcdrAlhueSgOP8+4K4pCSpOl0IpuOWrsKCmiKMP26mp2qYkh4vurFcYD9P9PmA/cY7oAdy9dM9JVoAb/H8Oh4UrrOmtcMpljK61h6x+XvaaMzo/ZF53BeA04Rc7TOytUsYde4ps1i6glbcSpyCOsJn6a+mLkPDfjOb7pwiSuCAdjC8T9UggMvsBkE8CqhAJLbeWtWixyc+OCpeiJmF2aCbNCwY03g+jsLg5qbZh38j3aaiHZrEuAGTOQVk7tcy+ovLDTbDgq3WrK9ixo16e+KbqycctRc/OJx/DXwmfbdIfP8AR4jBGJLsX1FgHVYwiXC5UjkOe4Zj19sgMGA796U10qMaPA+XD5tKKJVbfOYhHfce/Eo8ugY6o+w5+3cOvZqxw892B/klNap4QxKipPmZv4aFlUGnU23YCOZ/bT+TXIW5IQhXEVG8CMDiw9Xr+DaM5z2G3eg7+lpIn12ABBGSYxSXg4MyoW6T4ZdNgQwaOWannm9n3Cnxhz27NdCYo+3ZoPtNUj2Y6ckaf97dzvT08k88JEXRyv9X9RWJzJlFaj23H5R7tTM4TMJC7L32yTvsTO4Zs6rM=','EMPLOYEE', 'Dottie', 'Townsend'),
    (4, 'boss', 'password', 'MANAGER', 'Robert', 'Bauer');