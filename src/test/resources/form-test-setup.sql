drop table if exists reimbursement, profile;
create table profile (
	e_id int primary key,
	username varchar(50) unique not null,
	pass_key varchar(772) not null,
	access_level varchar(8) not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null
);
insert into profile (e_id, username, pass_key, access_level, first_name, last_name) values
    (1, 'user', 'password', 'EMPLOYEE', 'Alexa', 'Prince'),
    (2, 'user1', 'password', 'EMPLOYEE', 'Ciri', 'Schneewitchen'),
    (3, 'user3', 'password', 'EMPLOYEE', 'Dottie', 'Townsend'),
    (4, 'boss', 'password', 'MANAGER', 'Robert', 'Bauer');
create table reimbursement (
    	r_id int primary key,
    	employee int not null references profile(e_id),
    	file_date timestamp not null,
    	amount numeric(38, 2) not null,
    	status varchar(20) not null,
    	resolve_date timestamp,
    	manager int references profile(e_id)
);
insert into reimbursement(r_id, employee, file_date, amount, status) values
    (1, 1, '2020-09-30 04:30:22', 90.00, 'PENDING'),
    (2, 2, '2020-09-30 04:30:22', 70.35, 'PENDING'),
    (3, 3, '2020-09-30 04:30:22', 20.34, 'CANCELLED'),
    (4, 3, '2020-09-30 04:30:22', 105.45, 'PENDING');