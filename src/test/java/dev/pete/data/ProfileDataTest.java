package dev.pete.data;

import dev.pete.models.Profile;
import dev.pete.models.AccessLevel;
import dev.pete.utilities.SecurityServiceUtil;
import dev.pete.utilities.SessionServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;

import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class ProfileDataTest {
    private static final Logger logger = LogManager.getLogger(ProfileDataTest.class);
    private static final ProfileData profileData = new ProfileData();
    private static final Profile profile = new Profile();

    @BeforeAll
    static void establishDatabase() {
        SessionServiceUtil.load();
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            char[] buf = new char[1400];
            int i = new FileReader("src/test/resources/profile-test-setup.sql").read(buf);
            if (i==0) System.exit(i);
            session.createSQLQuery(String.valueOf(buf).trim()).executeUpdate();
            transaction.commit();
            SessionServiceUtil.flushAndClose(session);
        } catch (IOException e) {
            logger.error(e.getMessage());
            transaction.rollback();
        }
    }

    @Test
    void insertEmptyProfile() {
        int expected = -1;
        int actual = profileData.insertProfile(new Profile());
        assertEquals(expected, actual);
    }

    @Test
    void insertIncompleteProfile() {
        int expected = -1;

        profile.setId(SecurityServiceUtil.getId());
        profile.setUsername("incompletethatshouldnotbethere");
        profile.setPassKey("incomplete");
        profile.setAccessLevel(AccessLevel.MANAGER);
        profile.setFirstName(null);
        profile.setLastName(null);

        int actual = profileData.insertProfile(profile);
        assertEquals(expected, actual);
    }

    @Test
    void insertCompleteProfile() {
        int expected = 1;

        profile.setId(SecurityServiceUtil.getId());
        profile.setUsername("bestUserEver");
        profile.setPassKey("bestPassKeyEver");
        profile.setAccessLevel(AccessLevel.EMPLOYEE);
        profile.setFirstName("Queen");
        profile.setLastName("King");

        int actual = profileData.insertProfile(profile);

        assertEquals(expected, actual);
    }

    @Test
    void getExistingProfileById() {
        profile.setId(1);
        profile.setUsername("user");
        profile.setPassKey("password");
        profile.setAccessLevel(AccessLevel.EMPLOYEE);
        profile.setFirstName("Alexa");
        profile.setLastName("Prince");

        Profile expected = profile;
        Profile actual = profileData.getProfileById(profile.getId());

        assertEquals(expected, actual);
    }

    @Test
    void getExistingProfileByUsername() {
        profile.setId(2);
        profile.setUsername("user1");
        profile.setPassKey("password");
        profile.setAccessLevel(AccessLevel.EMPLOYEE);
        profile.setFirstName("Ciri");
        profile.setLastName("Schneewitchen");

        Profile expected = profile;
        Profile actual = profileData.getProfileById(profile.getId());

        assertEquals(expected, actual);
    }

    @Test
    void getProfileByFalseId() {
        assertNull(profileData.getProfileById(400));
    }

    @Test
    void getProfileByFalseUsername() {
        assertNull(profileData.getProfileByUsername("nonexistant"));
    }

    @Test
    void updateExistingProfile() {
        int expected = 1;

        profile.setId(4);
        profile.setUsername("boss");
        profile.setPassKey("password");
        profile.setAccessLevel(AccessLevel.EMPLOYEE);
        profile.setFirstName("Robert");
        profile.setLastName("Bauer");

        int actual = profileData.updateProfileById(profile);

        assertEquals(expected, actual);
    }

    @Test
    void updateNonexistentProfile() {
        int expected = -1;

        profile.setId(-2);
        profile.setUsername(null);
        profile.setPassKey(null);
        profile.setAccessLevel(null);
        profile.setFirstName(null);
        profile.setLastName(null);

        int actual = profileData.updateProfileById(profile);
        assertEquals(expected, actual);
    }

    @Test
    void updateProfileWithNullValues() {
        profile.setId(3);
        profile.setUsername(null);
        profile.setPassKey(null);
        profile.setAccessLevel(null);
        profile.setFirstName(null);
        profile.setLastName(null);

        int expected = -1;
        int acutal = profileData.updateProfileById(profile);
        assertEquals(expected, acutal);
    }

    @Test
    void deleteExistingProfile() {
        int expected = 1;
        int actual = profileData.deleteProfileById(3);
        assertEquals(expected, actual);
    }

    @Test
    void deleteNonexistentProfile() {
        int expected = 0;
        int actual = profileData.deleteProfileById(-2);
        assertEquals(expected, actual);
    }

    @Test
    void deleteNullId() {
        int expected = 0;
        int actual = profileData.deleteProfileById(null);
        assertEquals(expected, actual);
    }
}
