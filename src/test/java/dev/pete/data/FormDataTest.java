package dev.pete.data;

import dev.pete.models.Form;
import dev.pete.models.FormStatus;
import dev.pete.utilities.SecurityServiceUtil;
import dev.pete.utilities.SessionServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FormDataTest {
    private static final Logger logger = LogManager.getLogger(FormDataTest.class);
    private static final FormData formData = new FormData();
    private static Form form = new Form();

    @BeforeAll
    static void establishDatabase() {
        SessionServiceUtil.load();
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            char[] buf = new char[1400];
            int i = new FileReader("src/test/resources/form-test-setup.sql").read(buf);
            if (i==0) System.exit(i);
            session.createSQLQuery(String.valueOf(buf).trim()).executeUpdate();
            transaction.commit();
            SessionServiceUtil.flushAndClose(session);
        } catch (IOException e) {
            logger.error(e.getMessage());
            transaction.rollback();
        }
    }

    @Test
    void insertNullValue() {
        int expected = -1;
        int actual = formData.insertForm(null);
        assertEquals(expected, actual);
    }

    @Test
    void insertEmptyForm() {
        int expected = -1;
        int actual = formData.insertForm(new Form());
        assertEquals(expected, actual);
    }

    @Test
    void insertIncompleteForm() {
        int expected = -1;

        form.setId(SecurityServiceUtil.getId());
        form.setFileDate(null);
        form.setEmployee(null);
        form.setAmount(BigDecimal.TEN);
        form.setStatus(null);
        form.setManager(null);
        form.setResolveDate(null);

        int actual = formData.insertForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void insertPendingForm() {
        int expected = 1;

        form = new Form();
        form.setId(SecurityServiceUtil.getId());
        form.setFileDate(Timestamp.valueOf(LocalDateTime.now()));
        form.setEmployee(2);
        form.setAmount(BigDecimal.TEN);

        int actual = formData.insertForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void insertCompleteForm() {
        int expected = 1;

        form = new Form();
        form.setId(SecurityServiceUtil.getId());
        form.setFileDate(Timestamp.valueOf(LocalDateTime.now()));
        form.setEmployee(1);
        form.setAmount(BigDecimal.TEN);
        form.setManager(4);
        form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));

        int actual = formData.insertForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void getFormByNull() {
        assertNull(formData.getFormById(null));
    }

    @Test
    void getFormByIncorrectId() {
        assertNull(formData.getFormById(-1));
    }

    @Test
    void getExistingFormById() {
        form.setId(1);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.PENDING);
        form.setManager(null);
        form.setResolveDate(null);

        Form expected = form;
        Form actual = formData.getFormById(1);
        assertEquals(expected, actual);
    }

    @Test
    void getAllExistingForms() {
        List<Form> actual = formData.getAllForms();
        assertNotEquals(0, actual.size());
    }

    @Test
    void getExistingFormsByEmployee() {
        List<Form> expected = new ArrayList<>();

        Form form1 = new Form();
        form1.setId(3);
        form1.setEmployee(3);
        form1.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form1.setAmount(new BigDecimal(String.format("%.2f", 20.34)));
        form1.setStatus(FormStatus.CANCELLED);
        expected.add(form1);

        Form form2 = new Form();
        form2.setId(4);
        form2.setEmployee(3);
        form2.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form2.setAmount(new BigDecimal(String.format("%.2f", 105.45)));
        form2.setStatus(FormStatus.PENDING);
        expected.add(form2);

        List<Form> actual = formData.getFormsByEmployee(3);
        assertAll(
                () -> assertTrue(actual.contains(expected.get(0))),
                () -> assertTrue(actual.contains(expected.get(1))),
                () -> assertEquals(expected.size(), actual.size())
        );
    }

    @Test
    void getFormsByNonexistentEmployee() {
        assertTrue(formData.getFormsByEmployee(56).isEmpty());
    }

    @Test
    void getFormsByNullEmployee() {
        assertTrue(formData.getFormsByEmployee(null).isEmpty());
    }

    @Test
    void updateExistingForm() {
        int expected = 1;

        form.setId(2);
        form.setEmployee(2);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 70.35)));
        form.setStatus(FormStatus.APPROVED);
        form.setManager(4);
        form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));

        int actual = formData.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateNonExistentPendingForm() {
        int expected = -1;

        form.setId(72);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.PENDING);
        form.setManager(null);
        form.setResolveDate(null);

        int actual = formData.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateNonExistentRejectedForm() {
        int expected = -1;

        form.setId(72);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.REJECTED);
        form.setManager(4);
        form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));

        int actual = formData.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateFormRejectedMismatch() {
        int expected = -1;

        form.setId(72);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.REJECTED);
        form.setManager(null);
        form.setResolveDate(null);

        int actual = formData.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateNewForm() {
        int expected = -1;
        int actual = formData.updateForm(new Form());
        assertEquals(expected, actual);
    }

    @Test
    void updateNull() {
        int expected = -1;
        int actual = formData.updateForm(null);
        assertEquals(expected, actual);
    }

    @Test
    void deleteExistingForm() {
        int expected = 1;
        int actual = formData.deleteForm(3);
        assertEquals(expected, actual);
    }

    @Test
    void deleteNonExistentForm() {
        int expected = 0;
        int actual = formData.deleteForm(54);
        assertEquals(expected, actual);
    }

    @Test
    void deleteNullValue() {
        int expected = 0;
        int actual = formData.deleteForm(null);
        assertEquals(expected, actual);
    }
}
