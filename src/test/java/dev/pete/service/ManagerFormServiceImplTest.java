package dev.pete.service;

import dev.pete.models.*;
import dev.pete.services.FormService;
import dev.pete.services.FormServiceImpl;
import dev.pete.utilities.SecurityServiceUtil;
import dev.pete.utilities.SessionServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ManagerFormServiceImplTest {
    private static final Logger logger = LogManager.getLogger(ManagerFormServiceImplTest.class);
    private static FormService formService;
    private static Form form = new Form();
    private static final Profile profile = new Profile();

    @BeforeAll
    static void establishDatabase() {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            char[] buf = new char[1400];
            int i = new FileReader("src/test/resources/form-test-setup.sql").read(buf);
            if (i==0) System.exit(i);
            session.createSQLQuery(String.valueOf(buf).trim()).executeUpdate();
            transaction.commit();
            SessionServiceUtil.flushAndClose(session);
        } catch (IOException e) {
            logger.error(e.getMessage());
            transaction.rollback();
        }

        profile.setId(4);
        profile.setAccessLevel(AccessLevel.MANAGER);
        try {
            formService = new FormServiceImpl(profile);
        } catch (EmptyServiceException e) {
            e.printStackTrace();
        }
    }

    @Test
    void instantiateNullService() {
        assertThrows(NullPointerException.class, () -> new FormServiceImpl(null));
    }

    @Test
    void instantiateEmptyService() {
        assertThrows(EmptyServiceException.class, () -> new FormServiceImpl(new Profile()));
    }

    @Test
    void insertNullValue() {
        int expected = -1;
        int actual = formService.insertForm(null);
        assertEquals(expected, actual);
    }

    @Test
    void insertEmptyForm() {
        int expected = -1;
        int actual = formService.insertForm(new Form());
        assertEquals(expected, actual);
    }

    @Test
    void insertIncompleteForm() {
        int expected = -1;

        form.setId(SecurityServiceUtil.getId());
        form.setFileDate(null);
        form.setEmployee(null);
        form.setAmount(BigDecimal.TEN);
        form.setStatus(null);
        form.setManager(null);
        form.setResolveDate(null);

        int actual = formService.insertForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void insertPendingForm() {
        int expected = 1;

        form = new Form();
        form.setId(SecurityServiceUtil.getId());
        form.setFileDate(Timestamp.valueOf(LocalDateTime.now()));
        form.setEmployee(2);
        form.setAmount(BigDecimal.TEN);

        int actual = formService.insertForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void insertCompleteForm() {
        int expected = 1;

        form = new Form();
        form.setId(SecurityServiceUtil.getId());
        form.setFileDate(Timestamp.valueOf(LocalDateTime.now()));
        form.setEmployee(1);
        form.setAmount(BigDecimal.TEN);
        form.setStatus(FormStatus.APPROVED);
        form.setManager(4);
        form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));

        int actual = formService.insertForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void getFormByNull() {
        assertNull(formService.getForm(null));
    }

    @Test
    void getFormByNonexistentId() {
        assertNull(formService.getForm(-1));
    }

    @Test
    void getExistingFormById() {
        form.setId(1);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.PENDING);
        form.setManager(null);
        form.setResolveDate(null);

        Form expected = form;
        Form actual = formService.getForm(1);
        assertEquals(expected, actual);
    }

    @Test
    void getAllExistingForms() {
        List<Form> actual = formService.getAllForms();
        assertNotEquals(0, actual.size());
    }

    @Test
    void updateExistingForm() {
        int expected = 1;

        form.setId(2);
        form.setEmployee(2);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 70.35)));
        form.setStatus(FormStatus.APPROVED);
        form.setManager(4);
        form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));

        int actual = formService.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateNonExistentPendingForm() {
        int expected = -1;

        form.setId(72);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.PENDING);
        form.setManager(null);
        form.setResolveDate(null);

        int actual = formService.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateNonExistentRejectedForm() {
        int expected = -1;

        form.setId(72);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.REJECTED);
        form.setManager(4);
        form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));

        int actual = formService.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateFormRejectedMismatch() {
        int expected = -1;

        form.setId(72);
        form.setEmployee(1);
        form.setFileDate(Timestamp.valueOf("2020-09-30 04:30:22"));
        form.setAmount(new BigDecimal(String.format("%.2f", 90.0)));
        form.setStatus(FormStatus.REJECTED);
        form.setManager(null);
        form.setResolveDate(null);

        int actual = formService.updateForm(form);
        assertEquals(expected, actual);
    }

    @Test
    void updateNewForm() {
        int expected = -1;
        int actual = formService.updateForm(new Form());
        assertEquals(expected, actual);
    }

    @Test
    void updateNull() {
        int expected = -1;
        int actual = formService.updateForm(null);
        assertEquals(expected, actual);
    }

    @Test
    void deleteExistingForm() {
        int expected = 1;
        int actual = formService.deleteForm(3);
        assertEquals(expected, actual);
    }

    @Test
    void deleteNonExistentForm() {
        int expected = 0;
        int actual = formService.deleteForm(54);
        assertEquals(expected, actual);
    }

    @Test
    void deleteNullValue() {
        int expected = 0;
        int actual = formService.deleteForm(null);
        assertEquals(expected, actual);
    }
}
