package dev.pete.service;

import dev.pete.models.AccessLevel;
import dev.pete.models.Profile;
import dev.pete.services.ProfileService;
import dev.pete.services.ProfileServiceImpl;
import dev.pete.utilities.SecurityServiceUtil;
import dev.pete.utilities.SessionServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class ProfileServiceImplTest {
    private static final Logger logger = LogManager.getLogger(ProfileServiceImplTest.class);
    private static final ProfileService profileService = new ProfileServiceImpl();
    private static final Profile profile = new Profile();

    @BeforeAll
    static void establishDataBase() {
        SessionServiceUtil.load();
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            char[] buf = new char[1400];
            int i = new FileReader("src/test/resources/profile-test-setup.sql").read(buf);
            if (i==0) System.exit(i);
            session.createSQLQuery(String.valueOf(buf).trim()).executeUpdate();
            transaction.commit();
            SessionServiceUtil.flushAndClose(session);
        } catch (IOException e) {
            logger.error(e.getMessage());
            transaction.rollback();
        }
    }

    @Test
    void insertNullProfile() {
        int expected = -1;
        int actual = profileService.insertProfile(null);
        assertEquals(expected, actual);
    }

    @Test
    void insertEmptyProfile() {
        int expected = -1;
        int actual = profileService.insertProfile(new Profile());
        assertEquals(expected, actual);
    }

    @Test
    void insertCompleteProfile() {
        profile.setId(SecurityServiceUtil.getId());
        profile.setUsername("user4");
        profile.setPassKey("pass");
        profile.setAccessLevel(AccessLevel.MANAGER);
        profile.setFirstName("Elly");
        profile.setLastName("Emmy");

        int expected = 1;
        int actual = profileService.insertProfile(profile);
        assertEquals(expected, actual);
    }

    @Test
    void insertDuplicateProfile() {
        profile.setId(SecurityServiceUtil.getId());
        profile.setUsername("user7");
        profile.setPassKey("pass");
        profile.setAccessLevel(AccessLevel.MANAGER);
        profile.setFirstName("Elly");
        profile.setLastName("Emmy");
        profileService.insertProfile(profile);

        int expected = -1;
        int actual = profileService.insertProfile(profile);
        assertEquals(expected, actual);
    }

    @Test
    void getProfileByCorrectCredentials() {
        profile.setId(3);
        profile.setUsername("user3");
        profile.setPassKey("QMhsGjgRezMUv94r5XrBOt3s6tCf8qIosp7+tvRRXpQJwFkvoVouZWgftYSSaJXNomEW6WI3Taqf0es6uqEWTA==Zh8O/6MqKjWu5Nlmw3AFsFCm58E7inDkQOnoQ2cZ39Quakn9fP5NGv6RB/N1l2swkBhm70r31ZePMisnII3VqYvit40ANKpxPgcdrAlhueSgOP8+4K4pCSpOl0IpuOWrsKCmiKMP26mp2qYkh4vurFcYD9P9PmA/cY7oAdy9dM9JVoAb/H8Oh4UrrOmtcMpljK61h6x+XvaaMzo/ZF53BeA04Rc7TOytUsYde4ps1i6glbcSpyCOsJn6a+mLkPDfjOb7pwiSuCAdjC8T9UggMvsBkE8CqhAJLbeWtWixyc+OCpeiJmF2aCbNCwY03g+jsLg5qbZh38j3aaiHZrEuAGTOQVk7tcy+ovLDTbDgq3WrK9ixo16e+KbqycctRc/OJx/DXwmfbdIfP8AR4jBGJLsX1FgHVYwiXC5UjkOe4Zj19sgMGA796U10qMaPA+XD5tKKJVbfOYhHfce/Eo8ugY6o+w5+3cOvZqxw892B/klNap4QxKipPmZv4aFlUGnU23YCOZ/bT+TXIW5IQhXEVG8CMDiw9Xr+DaM5z2G3eg7+lpIn12ABBGSYxSXg4MyoW6T4ZdNgQwaOWannm9n3Cnxhz27NdCYo+3ZoPtNUj2Y6ckaf97dzvT08k88JEXRyv9X9RWJzJlFaj23H5R7tTM4TMJC7L32yTvsTO4Zs6rM=");
        profile.setAccessLevel(AccessLevel.EMPLOYEE);
        profile.setFirstName("Dottie");
        profile.setLastName("Townsend");

        Profile expected = profile;
        Profile actual = profileService.getProfileByCredentials("user3", "password");
        assertEquals(expected, actual);
    }

    @Test
    void getProfileByNonexistentCredentials() {
        assertNull(profileService.getProfileByCredentials("none", "nope"));
    }

    @Test
    void getProfileByNullCredentials() {
        assertNull(profileService.getProfileByCredentials(null, null));
    }

    @Test
    void getProfileByExistingId() {
        profile.setId(2);
        profile.setUsername("user1");
        profile.setPassKey("password");
        profile.setAccessLevel(AccessLevel.EMPLOYEE);
        profile.setFirstName("Ciri");
        profile.setLastName("Schneewitchen");

        Profile expected = profile;
        Profile actual = profileService.getProfileById(2);
        assertEquals(expected, actual);
    }

    @Test
    void getProfileByNonexistentId() {
        assertNull(profileService.getProfileById(90));
    }

    @Test
    void getProfileByNullId() {
        assertNull(profileService.getProfileById(null));
    }
}
