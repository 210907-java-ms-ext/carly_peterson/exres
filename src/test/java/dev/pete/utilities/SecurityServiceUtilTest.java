package dev.pete.utilities;

import dev.pete.models.AccessLevel;
import dev.pete.utilities.SecurityServiceUtil;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class SecurityServiceUtilTest {
    static String password;
    static String passKey;

    @BeforeAll
    static void setValues() {
        password = "pass";
        passKey = null;
    }

    @Test
    void hashPasswordUnique() {
        String use1 = SecurityServiceUtil.hashPassword(password);
        String use2 = SecurityServiceUtil.hashPassword(password);
        assertNotEquals(use1, use2);
    }

    @Test
    void hashEmptyString() {
        assertNull(SecurityServiceUtil.hashPassword(""));
    }

    @Test
    void hashNull() {
        assertNull(SecurityServiceUtil.hashPassword(null));
    }

    @Test
    void correctlyUnhashPassword() {
        String passKey1 = SecurityServiceUtil.hashPassword(password);
        assertTrue(SecurityServiceUtil.isPassword(password, passKey1));
    }

    @Test
    void unhashNullPassword() {
        assertFalse(SecurityServiceUtil.isPassword(null, null));
    }

    @Test
    void unhashEmptyString() {
        assertFalse(SecurityServiceUtil.isPassword("", ""));
    }

    @Test
    void generateCorrectIdSize() {
        Integer id = SecurityServiceUtil.getId();
        int len = id.toString().length();
        assertAll(
                () -> assertTrue(len >= 9),
                () -> assertTrue(len < 11)
        );
    }

    @Test
    void generateUniqueIds() {
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < 10000; i++){
            ids.add(SecurityServiceUtil.getId());
        }
        List<Integer> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        assertEquals(ids.size(), distinctIds.size());
    }

    @Test
    void generateToken() {
        Integer id = 80;
        AccessLevel accessLevel = AccessLevel.MANAGER;
        assertNotNull(SecurityServiceUtil.generateToken(id, accessLevel));
    }

    @Test
    void generateTokenFromNull() {
        assertNull(SecurityServiceUtil.generateToken(null, null));
    }

    @Test
    void correctlyValidateToken() {
        Integer id = 42;
        AccessLevel accessLevel = AccessLevel.MANAGER;
        String token = SecurityServiceUtil.generateToken(id, accessLevel);
        HashMap<String, Object> values = SecurityServiceUtil.validateToken(token);
        assertAll(
                () -> assertNotNull(values),
                () -> assertEquals(2, values.size()),
                () -> assertEquals(id, values.get("id")),
                () -> assertEquals(accessLevel, values.get("access_level"))
        );
    }

    @Test
    void validateNullToken() {
        assertNull(SecurityServiceUtil.validateToken(null));
    }

    @Test
    void validateEmptyToken() {
        assertNull(SecurityServiceUtil.validateToken(""));
    }
}
