package dev.pete.models;

public enum FormStatus {
    PENDING, APPROVED, REJECTED, CANCELLED
}
