package dev.pete.models;

public class EmptyServiceException extends Exception {
    @Override
    public String getMessage() {
        return "FormServiceImpl constructor profile was Empty.";
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }
}
