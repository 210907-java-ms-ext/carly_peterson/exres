package dev.pete.models;

public enum AccessLevel {
    EMPLOYEE, MANAGER
}