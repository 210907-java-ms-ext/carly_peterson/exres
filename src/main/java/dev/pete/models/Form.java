package dev.pete.models;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "reimbursement")
public class Form implements Serializable {
    @Id
    @Column(name = "r_id")
    private Integer id;

    @Column(name = "employee")
    private Integer employee;

    @Column(name = "file_date")
    private Timestamp fileDate;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private FormStatus status;

    @Column(name = "resolve_date")
    private Timestamp resolveDate;

    @Column(name = "manager")
    private Integer manager;

    public Form() {
        super();
        status = FormStatus.PENDING;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmployee() {
        return employee;
    }

    public void setEmployee(Integer employee) {
        this.employee = employee;
    }

    public Timestamp getFileDate() {
        return fileDate;
    }

    public void setFileDate(Timestamp fileDate) {
        this.fileDate = fileDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public FormStatus getStatus() {
        return status;
    }

    public void setStatus(FormStatus status) {
        this.status = status;
    }

    public Timestamp getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Timestamp resolveDate) {
        this.resolveDate = resolveDate;
    }

    public Integer getManager() {
        return manager;
    }

    public void setManager(Integer manager) {
        this.manager = manager;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return id.equals(form.id) &&
                employee.equals(form.employee) &&
                fileDate.equals(form.fileDate) &&
                amount.equals(form.amount) &&
                status == form.status &&
                Objects.equals(resolveDate, form.resolveDate) &&
                Objects.equals(manager, form.manager);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employee, fileDate, amount, status, resolveDate, manager);
    }

    @Override
    public String toString() {
        return "Form{" +
                "id=" + id +
                ", employee=" + employee +
                ", fileDate=" + fileDate +
                ", amount=" + amount +
                ", status=" + status +
                ", resolvedDate=" + resolveDate +
                ", manager=" + manager +
                '}';
    }
}
