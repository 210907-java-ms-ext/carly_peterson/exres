package dev.pete.data;

import dev.pete.models.Profile;
import dev.pete.models.AccessLevel;
import dev.pete.utilities.ConnectionServiceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 * A data layer class that directly access and manipulates the profile side of the database
 */
@Deprecated
public class ProfileDataJDBC implements ProfileDataDAO{
    public ProfileDataJDBC() {
        super();
    }

    /**
     * Retrieves profile with the given id from the database
     * @param id unique number associated with a profile
     * @return retrieved Profile, null if unsuccessful
     */
    @Override
    public Profile getProfileById(Integer id) {
        String cmd = "select * from profile where e_id = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            Profile profile = new Profile();
            profile.setId(id);
            profile.setUsername(rs.getString("username"));
            profile.setPassKey(rs.getString("pass_key"));
            profile.setAccessLevel(AccessLevel.valueOf(rs.getString("access_level")));
            profile.setFirstName(rs.getString("first_name"));
            profile.setLastName(rs.getString("last_name"));
            return profile;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return null;
    }

    /**
     * Inserts the given profile into the database
     * @param profile data to be entered
     * @return 1 if successful
     */
    @Override
    public int insertProfile(Profile profile) {
        String cmd = "insert into profile (e_id, username, pass_key, access_level, first_name, last_name) " +
                "values (?, ?, ?, ?, ?, ?)";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, profile.getId());
            ps.setString(2, profile.getUsername());
            ps.setString(3, profile.getPassKey());
            ps.setString(4, String.valueOf(profile.getAccessLevel()));
            ps.setString(5, profile.getFirstName());
            ps.setString(6, profile.getLastName());
            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error("Profile not persisted " + e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }

    /**
     * Updates an existing profile using the given profile data
     * @param profile data to replace the existing profile
     * @return 1 if successful
     */
    @Override
    public int updateProfileById(Profile profile) {
        String cmd = "update profile set (e_id, username, pass_key, access_level, first_name, last_name) " +
                "= (?, ?, ?, ?, ?, ?) where e_id = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, profile.getId());
            ps.setString(2, profile.getUsername());
            ps.setString(3, profile.getPassKey());
            ps.setString(4, String.valueOf(profile.getAccessLevel()));
            ps.setString(5, profile.getFirstName());
            ps.setString(6, profile.getLastName());
            ps.setInt(7, profile.getId());
            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error("Profile not persisted " + e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }

    /**
     * Deletes a profile from the database using the given id
     * @param id unique number associate with a profile
     * @return 1 if successful
     */
    @Override
    public int deleteProfileById(Integer id) {
        String cmd = "delete from profile where e_id = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, id);
            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error("Profile not persisted " + e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }

    /**
     * Retrieves a profile from the database using the given username
     * @param username unique key associate with one profile
     * @return Profile with given username, null if unsuccessful
     */
    @Override
    public Profile getProfileByUsername(String username) {
        String cmd = "Select * from profile where username = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) return null;
            Profile profile = new Profile();
            profile.setId(rs.getInt("e_id"));
            profile.setUsername(rs.getString("username"));
            profile.setPassKey(rs.getString("pass_key"));
            profile.setAccessLevel(AccessLevel.valueOf(rs.getString("access_level")));
            profile.setFirstName(rs.getString("first_name"));
            profile.setLastName(rs.getString("last_name"));
            return profile;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return null;
    }

    /**
     * Retrieves all the current profiles from the database
     * @return list of all current profiles
     */
    @Override
    public List<Profile> getAllProfiles() {
        String cmd = "select * from profile";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ResultSet rs = ps.executeQuery();
            List<Profile> profiles = new LinkedList<>();
            while (rs.next()) {
                Profile profile = new Profile();
                profile.setId(rs.getInt("e_id"));
                profile.setUsername(rs.getString("username"));
                profile.setPassKey(rs.getString("pass_key"));
                profile.setAccessLevel(AccessLevel.valueOf(rs.getString("access_level")));
                profile.setFirstName(rs.getString("first_name"));
                profile.setLastName(rs.getString("last_name"));
                profiles.add(profile);
            }
            return profiles;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return null;
    }
}
