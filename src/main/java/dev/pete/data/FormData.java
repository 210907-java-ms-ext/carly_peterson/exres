package dev.pete.data;

import dev.pete.models.Form;
import dev.pete.utilities.SessionServiceUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * A data layer class that directly access and alters the forms side of the database
 */
public class FormData implements FormDataDAO {
    public FormData() {
        super();
    }

    /**
     * Retrieves a form from the database using the given id
     * @param id the unique id of the requested form
     * @return Form with given id, null if unsuccessful
     */
    @Override
    public Form getFormById(Integer id) {
        Session session = SessionServiceUtil.getSession();
        try {
            return session.get(Form.class, id);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return null;
    }

    /**
     * Retrieves all the forms in the database
     * @return a List of all existing forms
     */
    @Override
    public List<Form> getAllForms() {
        Session session = SessionServiceUtil.getSession();
        try {
            CriteriaQuery<Form> cq = session.getCriteriaBuilder().createQuery(Form.class);
            cq.from(Form.class);
            Query query = session.createQuery(cq);
            return query.getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return null;
    }

    /**
     * Retrieves all the forms in the database with the given employee value
     * @param id employee value
     * @return list of forms with employee id
     */
    @Override
    public List<Form> getFormsByEmployee(Integer id) {
        Session session = SessionServiceUtil.getSession();
        try {
            Query query = session.createQuery("from Form where employee = :id");
            query.setParameter("id", id);
            return query.getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return null;
    }

    /**
     * Inserts a form into the database
     * @param form the form that should be inserted
     * @return 1 if successful
     */
    @Override
    public int insertForm(Form form) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(form);
            transaction.commit();
            return 1;
        } catch (Exception e) {
            logger.error(e.getMessage());
            transaction.rollback();
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }

    /**
     * Updates an existing form in the database using the given form's data
     * @param form the data that should replace what is in the database
     * @return 1 if successful
     */
    @Override
    public int updateForm(Form form) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(form);
            transaction.commit();
            return 1;
        } catch (Exception e) {
            logger.error(e.getMessage());
            transaction.rollback();
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }

    /**
     * Deletes the form with the given id from the database
     * @param id unique id associated with a form
     * @return 1 if successful
     */
    @Override
    public int deleteForm(Integer id) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            Query query = session.createQuery("delete Form where r_id = :id");
            query.setParameter("id", id);
            int i = query.executeUpdate();
            transaction.commit();
            return i;
        } catch (Exception e) {
            logger.error(e.getMessage());
            transaction.rollback();
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }

    /**
     * Changes the status of an entry to cancelled and checks that the employee requesting it has the permission to do so
     * @param rid form id
     * @param eid employee id
     * @return 1 if successful
     */
    @Override
    public int cancelForm(Integer rid, Integer eid) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            Query query = session.createQuery("update Profile set status = CANCELLED " +
                                                    "where (r_id = :rid and employee = :eid)");
            query.setParameter("rid", rid);
            query.setParameter("eid", eid);
            int i = query.executeUpdate();
            transaction.commit();
            return i;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }
}
