package dev.pete.data;

import dev.pete.models.Form;
import dev.pete.models.FormStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

public interface FormDataDAO {
    Logger logger = LogManager.getLogger(FormData.class);
    Form getFormById(Integer id);
    List<Form> getAllForms();
    List<Form> getFormsByEmployee(Integer id);
    int insertForm(Form form);
    int updateForm(Form form);
    int deleteForm(Integer id);
    int cancelForm(Integer r_id, Integer e_id);
}
