package dev.pete.data;

import dev.pete.models.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface ProfileDataDAO {
    Logger logger = LogManager.getLogger(ProfileData.class);
    Profile getProfileById(Integer id);
    Profile getProfileByUsername(String username);
    List<Profile> getAllProfiles();
    int insertProfile(Profile profile);
    int updateProfileById(Profile profile);
    int deleteProfileById(Integer id);
}
