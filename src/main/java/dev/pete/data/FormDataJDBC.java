package dev.pete.data;

import dev.pete.models.Form;
import dev.pete.models.FormStatus;
import dev.pete.utilities.ConnectionServiceUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Deprecated
/**
 * A data layer class that directly access and alters the forms side of the database
 */
public class FormDataJDBC implements FormDataDAO {
    public FormDataJDBC() {
        super();
    }

    /**
     * Retrieves a form from the database using the given id
     * @param id the unique id of the requested form
     * @return Form with given id, null if unsuccessful
     */
    @Override
    public Form getFormById(Integer id) {
        String cmd = "select * from reimbursement where r_id = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            Form form = new Form();
            form.setId(id);
            form.setEmployee(rs.getInt("employee"));
            form.setFileDate(rs.getTimestamp("file_date"));
            form.setAmount(rs.getBigDecimal("amount"));
            form.setStatus(FormStatus.valueOf(rs.getString("status")));
            if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                form.setResolveDate(rs.getTimestamp("resolve_date"));
                form.setManager(rs.getInt("manager"));
            }
            return form;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return null;
    }

    /**
     * Retrieves all the forms in the database
     * @return a List of all existing forms
     */
    @Override
    public List<Form> getAllForms() {
        String cmd = "select * from reimbursement";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ResultSet rs = ps.executeQuery();
            ArrayList<Form> forms = new ArrayList<>();
            while(rs.next()) {
                Form form = new Form();
                form.setId(rs.getInt("r_id"));
                form.setEmployee(rs.getInt("employee"));
                form.setFileDate(rs.getTimestamp("file_date"));
                form.setAmount(rs.getBigDecimal("amount"));
                form.setStatus(FormStatus.valueOf(rs.getString("status")));
                if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                    form.setResolveDate(rs.getTimestamp("resolve_date"));
                    form.setManager(rs.getInt("manager"));
                }
                forms.add(form);
            }
            return forms;
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return null;
    }

    /**
     * Retrieves all the forms in the database with the given employee value
     * @param id employee value
     * @return list of forms with employee id
     */
    @Override
    public List<Form> getFormsByEmployee(Integer id) {
        String cmd = "select * from reimbursement where employee = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            ArrayList<Form> forms = new ArrayList<>();
            while(rs.next()) {
                Form form = new Form();
                form.setId(rs.getInt("r_id"));
                form.setEmployee(rs.getInt("employee"));
                form.setFileDate(rs.getTimestamp("file_date"));
                form.setAmount(rs.getBigDecimal("amount"));
                form.setStatus(FormStatus.valueOf(rs.getString("status")));
                if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                    form.setResolveDate(rs.getTimestamp("resolve_date"));
                    form.setManager(rs.getInt("manager"));
                }
                forms.add(form);
            }
            return forms;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return null;
    }

    /**
     * Inserts a form into the database
     * @param form the form that should be inserted
     * @return 1 if successful
     */
    @Override
    public int insertForm(Form form) {
        String cmd = "insert into reimbursement (r_id, employee, file_date, amount, status) " +
                "values (?, ?, ?, ?, ?)";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                cmd = "insert into reimbursement (r_id, employee, file_date, amount, status, resolve_date, manager) " +
                        "values (?, ?, ?, ?, ? ,?, ?)";
            }
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, form.getId());
            ps.setInt(2, form.getEmployee());
            ps.setTimestamp(3, form.getFileDate());
            ps.setBigDecimal(4, form.getAmount());
            ps.setString(5, String.valueOf(form.getStatus()));
            if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                ps.setTimestamp(6, form.getResolveDate());
                ps.setInt(7, form.getManager());
            }
            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }

    /**
     * Updates an existing form in the database using the given form's data
     * @param form the data that should replace what is in the database
     * @return 1 if successful
     */
    @Override
    public int updateForm(Form form) {
        String cmd = "update reimbursement set (r_id, employee, file_date, amount, status) =" +
                " (?, ?, ?, ?, ?) where r_id = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                cmd = "update reimbursement set (r_id, employee, file_date, amount, status, resolve_date, manager) =" +
                        " (?, ?, ?, ?, ? ,?, ?) where r_id = ?";
            }
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, form.getId());
            ps.setInt(2, form.getEmployee());
            ps.setTimestamp(3, form.getFileDate());
            ps.setBigDecimal(4, form.getAmount());
            ps.setString(5, String.valueOf(form.getStatus()));
            if (form.getStatus() != FormStatus.PENDING && form.getStatus() != FormStatus.CANCELLED) {
                ps.setTimestamp(6, form.getResolveDate());
                ps.setInt(7, form.getManager());
                ps.setInt(8, form.getId());
            } else {
                ps.setInt(6, form.getId());
            }
            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }

    /**
     * Deletes the form with the given id from the database
     * @param id unique id associated with a form
     * @return 1 if successful
     */
    @Override
    public int deleteForm(Integer id) {
        String cmd = "delete from reimbursement where r_id = ?";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, id);
            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }

    /**
     * Changes the status of an entry to cancelled and checks that the employee requesting it has the permission to do so
     * @param r_id form id
     * @param e_id employee id
     * @return 1 if successful
     */
    @Override
    public int cancelForm(Integer r_id, Integer e_id) {
        String cmd = "update reimbursement set status = CANCELLED where r_id = ? && employee = ?;";
        Connection c = ConnectionServiceUtil.getConnection();
        try {
            PreparedStatement ps = c.prepareStatement(cmd);
            ps.setInt(1, r_id);
            ps.setInt(2, e_id);

            int status = ps.executeUpdate();
            c.commit();
            return status;
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionServiceUtil.releaseConnection(c);
        }
        return -1;
    }
}
