package dev.pete.data;

import dev.pete.models.Profile;
import dev.pete.utilities.SessionServiceUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * A data layer class that directly access and manipulates the profile side of the database
 */
public class ProfileData implements ProfileDataDAO {
    public ProfileData() {
        super();
    }

    /**
     * Retrieves profile with the given id from the database
     * @param id unique number associated with a profile
     * @return retrieved Profile, null if unsuccessful
     */
    @Override
    public Profile getProfileById(Integer id) {
        Session session = SessionServiceUtil.getSession();
        try {
            return session.get(Profile.class, id);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return null;
    }

    /**
     * Inserts the given profile into the database
     * @param profile data to be entered
     * @return 1 if successful
     */
    @Override
    public int insertProfile(Profile profile) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(profile);
            transaction.commit();
            return 1;
        } catch (Exception e) {
            logger.error(e.getMessage());
            transaction.rollback();
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }

    /**
     * Updates an existing profile using the given profile data
     * @param profile data to replace the existing profile
     * @return 1 if successful
     */
    @Override
    public int updateProfileById(Profile profile) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(profile);
            transaction.commit();
            return 1;
        } catch (Exception e) {
            logger.error(e.getMessage());
            transaction.rollback();
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }

    /**
     * Deletes a profile from the database using the given id
     * @param id unique number associate with a profile
     * @return 1 if successful
     */
    @Override
    public int deleteProfileById(Integer id) {
        Session session = SessionServiceUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            Query query = session.createQuery("delete Profile where e_id = :id");
            query.setParameter("id", id);
            int i = query.executeUpdate();
            transaction.commit();
            return i;
        } catch (Exception e) {
            logger.error(e.getMessage());
            transaction.rollback();
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return -1;
    }

    /**
     * Retrieves a profile from the database using the given username
     * @param username unique key associate with one profile
     * @return Profile with given username, null if unsuccessful
     */
    @Override
    public Profile getProfileByUsername(String username) {
        Session session = SessionServiceUtil.getSession();
        try {
            Query query = session.createQuery("from Profile where username = :username");
            query.setParameter("username", username);
            return (Profile) query.getResultList().get(0);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return null;
    }

    /**
     * Retrieves all the current profiles from the database
     * @return list of all current profiles
     */
    @Override
    public List<Profile> getAllProfiles() {
        Session session = SessionServiceUtil.getSession();
        try {
            CriteriaQuery<Profile> cq = session.getCriteriaBuilder().createQuery(Profile.class);
            cq.from(Profile.class);
            Query query = session.createQuery(cq);
            return query.getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            SessionServiceUtil.flushAndClose(session);
        }
        return null;
    }
}
