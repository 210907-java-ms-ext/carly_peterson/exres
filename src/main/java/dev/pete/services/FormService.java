package dev.pete.services;

import dev.pete.data.FormData;
import dev.pete.data.FormDataDAO;
import dev.pete.models.Form;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface FormService extends AutoCloseable {
    Logger logger = LogManager.getLogger(FormService.class);
    FormDataDAO formData = new FormData();
    int insertForm(Form form);
    Form getForm(Integer id);
    List<Form> getAllForms();
    int updateForm(Form form);
    int deleteForm(Integer id);
}
