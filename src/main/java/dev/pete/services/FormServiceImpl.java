package dev.pete.services;

import dev.pete.models.*;

import java.math.BigDecimal;
import java.util.List;

public class FormServiceImpl implements FormService {
    private final Integer userId;
    private final AccessLevel accessLevel;

    public FormServiceImpl(Profile profile) throws EmptyServiceException {
        if (profile.getId() == null || profile.getAccessLevel() == null) {
            throw new EmptyServiceException();
        }
        userId = profile.getId();
        accessLevel = profile.getAccessLevel();
    }

    /**
     * Checks weather the user of the instance is a manger or not
     * @return true when manager, false otherwise
     */
    private boolean isEmployee() {
        return accessLevel.equals(AccessLevel.EMPLOYEE);
    }

    /**
     * Inserts the given form into the current records
     * @param form form to add to record
     * @return 1 if successful
     */
    @Override
    public int insertForm(Form form) {
        if (form == null || form.getId() == null || form.getEmployee() == null || form.getFileDate() == null ||
            form.getAmount() == null || form.getStatus() == null) {
            logger.error("Cannot insert records with key values left incomplete.");
            return -1;
        }
        if (isEmployee() && form.getStatus() != FormStatus.PENDING) {
            logger.error("You do not have the necessary access level to insert a completed record.");
            return -1;
        }
        return formData.insertForm(form);
    }

    /**
     * Returns the form with the requested id.
     * @param id the form to be retrieved
     * @return the form, null if user does not have appropriate access levels
     */
    @Override
    public Form getForm(Integer id) {
        Form form = formData.getFormById(id);
        if (form != null && isEmployee() && !userId.equals(form.getEmployee())) {
            logger.error("You do not have the necessary access level to view the requested record.");
            return null;
        }
        return form;
    }

    /**
     * Retrieves the full list of forms for manager level user
     * @return list of all forms, null if user does not have appropriate access levels
     */
    @Override
    public List<Form> getAllForms() {
        if (isEmployee()) {
            return formData.getFormsByEmployee(userId);
        }
        return formData.getAllForms();
    }

    /**
     * Updates the given form in the current records
     * @param form form to update
     * @return 1 if successful
     */
    @Override
    public int updateForm(Form form) {
        if (form == null || form.getId() == null || form.getEmployee() == null || form.getFileDate() == null ||
                form.getAmount().equals(BigDecimal.ONE) || form.getStatus() == null) {
            logger.error("Cannot update records with key values left incomplete.");
            return -1;
        }
        if (isEmployee() &&
                (!userId.equals(form.getEmployee()) ||
                form.getStatus() == FormStatus.APPROVED ||
                form.getStatus() == FormStatus.REJECTED)) {
            logger.error("You do not have the necessary access level to update the requested record.");
            return -1;
        }
        return formData.updateForm(form);
    }

    /**
     * Deletes the form with the given id from the current records for managers,
     * or cancels the form for employees
     * @param fid form to be deleted
     * @return 1 if successful
     */
    @Override
    public int deleteForm(Integer fid) {
        if (isEmployee()) {
            return formData.cancelForm(fid, userId);
        }
        return formData.deleteForm(fid);
    }

    @Override
    public void close() {
        logger.info("FormService instance closed");
    }
}
