package dev.pete.services;

import dev.pete.models.Profile;
import dev.pete.utilities.SecurityServiceUtil;

import java.util.List;

public class ProfileServiceImpl implements ProfileService {
    /**
     * Inserts the given profile into the current records
     * @param profile to be inserted
     * @return 1 if successful
     */
    @Override
    public int insertProfile(Profile profile) {
        if (profile == null || profile.getId() == null || profile.getUsername().isEmpty() || profile.getPassKey().isEmpty() ||
            profile.getAccessLevel() == null || profile.getFirstName() == null || profile.getLastName() == null) {
            logger.error("Cannot insert profile with key values left incomplete.");
            return -1;
        }
        return profileData.insertProfile(profile);
    }

    /**
     * Gets the profile with the given username and password combo
     * @param username string used to identify the correct profile
     * @param password used to validate the retrieved profile is correct
     * @return the retrieved profile, null if profile not found
     */
    public Profile getProfileByCredentials(String username, String password) {
        if (username != null && !username.isEmpty() && password != null && !password.isEmpty()) {
            Profile profile = profileData.getProfileByUsername(username);
            if (profile == null) return null;
            if (SecurityServiceUtil.isPassword(password, profile.getPassKey())) return profile;
        }
        return null;
    }

    /**
     * Gets a profile with the given id
     * @param id profile to retrieve
     * @return the associated profile
     */
    public Profile getProfileById(Integer id) {
        return profileData.getProfileById(id);
    }

    /**
     * Retrieves all the current profile records
     * @return list of all current profiles
     */
    @Override
    public List<Profile> getAllProfiles() {
        return profileData.getAllProfiles();
    }

    @Override
    public int deleteProfileById(Integer id) {
        return profileData.deleteProfileById(id);
    }
}
