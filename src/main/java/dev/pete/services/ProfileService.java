package dev.pete.services;

import dev.pete.data.ProfileData;
import dev.pete.data.ProfileDataDAO;
import dev.pete.models.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface ProfileService {
    Logger logger = LogManager.getLogger(ProfileServiceImpl.class);
    ProfileDataDAO profileData = new ProfileData();
    int insertProfile(Profile profile);
    Profile getProfileByCredentials(String username, String password);
    Profile getProfileById(Integer id);
    List<Profile> getAllProfiles();
    int deleteProfileById(Integer id);
}
