package dev.pete.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Deprecated
public class ConnectionServiceUtil {
    private static final Logger logger = LogManager.getLogger(ConnectionServiceUtil.class);
    private static final int POOL_SIZE = 10;
    private static final List<Connection> pool = new LinkedList<>();
    private static final List<Connection> used = new LinkedList<>();

    static {
        try {
            String url;
            if (Boolean.parseBoolean(System.getenv("EXRES_TEST_ENV"))) {
                url = "jdbc:h2:~/test";
                pool.add(DriverManager.getConnection(url));
            } else {
                url = System.getenv("EXRES_URL");
                Class.forName("org.postgresql.Driver");
                logger.info("establishing database connection pool...");
                for (int i = 0; i < POOL_SIZE; i++) {
                    pool.add(DriverManager.getConnection(url, System.getenv("EXRES_USER"), System.getenv("EXRES_PASS")));
                    pool.get(i).setAutoCommit(false);
                }
            }
            logger.info("all connections successfully established");
        } catch (ClassNotFoundException | SQLException e) {
            logger.error("unable to finish establishing all database connections " + e.getMessage());
        }
    }

    /**
     * Pulls an unused connection from the pool
     * @return the pulled connection
     */
    public static Connection getConnection() {
        Connection c = pool.get(0);
        used.add(c);
        logger.info("connection successfully retrieved from pool");
        return c;
    }

    /**
     * Moves given connection back into the pool of usable connections
     * @param c the connection that was previously used
     * @return true if connection release, false if unable to release connection
     */
    public static boolean releaseConnection(Connection c) {
        try {c.rollback();}
        catch (SQLException ex){logger.error("Unable to rollback transaction " + ex.getMessage());}
        pool.add(POOL_SIZE - 1, c);
        return used.remove(c);
    }
}
