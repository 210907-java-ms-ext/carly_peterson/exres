package dev.pete.utilities;

import dev.pete.models.Form;
import dev.pete.models.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class SessionServiceUtil {
    private static final Logger logger  = LogManager.getLogger(SessionServiceUtil.class);
    private static SessionFactory sessionFactory;

    public static void load() {
        logger.info("Building Sesssion Factory...");
        Configuration configuration = new Configuration()
                .addPackage("dev.pete.data")
                .addAnnotatedClass(Form.class)
                .addAnnotatedClass(Profile.class)
                .configure();
        if (!Boolean.parseBoolean(System.getenv("EXRES_TEST_ENV"))) {
            configuration.setProperty("hibernate.connection.url", System.getenv("EXRES_URL"));
            configuration.setProperty("hibernate.connection.username", System.getenv("EXRES_USER"));
            configuration.setProperty("hibernate.connection.password", System.getenv("EXRES_PASS"));
        } else {
            configuration.setProperty("hibernate.connection.url", "jdbc:h2:~/test");
            configuration.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
            configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        }
        ServiceRegistry serviceRegistry =
                new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        logger.info("Session Factory Successfully Instantiated.");
    }

    public static Session getSession() {
        logger.info("New Session Opened");
        return sessionFactory.openSession();
    }

    public static void flushAndClose(Session session) {
        if (session.getTransaction().isActive()) session.flush();
        session.close();
        logger.info("Session Flushed and Closed");
    }
}
