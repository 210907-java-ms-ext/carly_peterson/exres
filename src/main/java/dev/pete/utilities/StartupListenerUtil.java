package dev.pete.utilities;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartupListenerUtil implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SessionServiceUtil.load();
    }
}
