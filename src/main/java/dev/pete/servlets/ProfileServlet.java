package dev.pete.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.pete.models.AccessLevel;
import dev.pete.models.Profile;
import dev.pete.services.ProfileService;
import dev.pete.services.ProfileServiceImpl;
import dev.pete.utilities.SecurityServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

public class ProfileServlet extends HttpServlet {
    private final static String employeeURL = "/EXRESEL/employee.html";
    private final static String managerURL = "/EXRESEL/manager.html";
    private final static Logger logger = LogManager.getLogger(ProfileServlet.class);
    private final ProfileService profileService = new ProfileServiceImpl();

    /**
     * Attempt to generate a token for the username and password from the request.
     * 200 -- ok, token returned in header
     * 405 -- username and password incorrect
     * 500 -- exception
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST request received by ProfileServlet");

        // Retrieve Parameters
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        // Check credentials and retrieve profile
        Profile profile = profileService.getProfileByCredentials(username, password);

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        try (PrintWriter pw = resp.getWriter()) {
            if (profile == null) { // Incorrect/Nonexistent credentials
                logger.error("Failed to find Profile with the given credentials");
                pw.write("#");
                resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            } else {
                logger.info("Profile successfully found for given credentials");
                String token = SecurityServiceUtil.generateToken(profile.getId(), profile.getAccessLevel());
                resp.setHeader("Authorization", token);
                // Redirect link depending on access level
                if (profile.getAccessLevel() == AccessLevel.MANAGER) {
                    pw.write(managerURL);
                } else {
                    pw.write(employeeURL);
                }
                resp.setStatus(HttpServletResponse.SC_OK);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("POST request successfully resolved for ProfileServlet");
    }

    /**
     * Inserts the new profile generated from the request and generates an equivalent token.
     * 200 -- profile inserted, token returned in header
     * 400 -- incorrect body format
     * 409 -- username already taken, unable to insert
     * 500 -- exception
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("PUT request received by ProfileServlet.");

        // Get body data string
        BufferedReader bufRead = req.getReader();
        String username = bufRead.readLine();
        String password = bufRead.readLine();
        String firstName = bufRead.readLine();
        String lastName = bufRead.readLine();

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (username.isEmpty() || password.isEmpty() || firstName.isEmpty() || lastName.isEmpty()) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            try {
                Profile profile = new Profile();
                profile.setId(SecurityServiceUtil.getId());
                profile.setUsername(username);
                profile.setPassKey(SecurityServiceUtil.hashPassword(password));
                profile.setAccessLevel(AccessLevel.EMPLOYEE);
                profile.setFirstName(firstName);
                profile.setLastName(lastName);

                try (PrintWriter pw = resp.getWriter()) {
                    if (profileService.insertProfile(profile) != 1) { // Profile was not inserted
                        logger.error("Unable to add new profile with given credentials.");
                        pw.write("#");
                        resp.setStatus(HttpServletResponse.SC_CONFLICT);
                    } else {
                        String token = SecurityServiceUtil.generateToken(profile.getId(), profile.getAccessLevel());
                        resp.setHeader("Authorization", token);
                        pw.write(employeeURL);
                        resp.setStatus(HttpServletResponse.SC_OK);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            } catch (IndexOutOfBoundsException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        logger.info("PUT request successfully resolved for ProfileServlet");
    }

    /**
     * Returns a list of all the current profiles if the request token has the proper authorization.
     * 200 -- list of profiles returned in body
     * 400 -- token not valid
     * 404 -- could not find profile
     * 405 -- request does not have sufficient access level
     * 500 -- exception
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET request received by ProfileServlet.");

        // Validate token
        String token = req.getHeader("Authorization");
        HashMap<String, Object> keys = SecurityServiceUtil.validateToken(token);

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (keys == null || keys.isEmpty()) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            Profile profile = profileService.getProfileById((Integer) keys.get("id"));
            if (profile == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            } else {
                if (profile.getAccessLevel() != AccessLevel.MANAGER) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    resp.getWriter().write(objectMapper.writeValueAsString(profile));
                } else {
                    List<Profile> profiles = profileService.getAllProfiles();
                    ObjectMapper objectMapper = new ObjectMapper();
                    resp.getWriter().write(objectMapper.writeValueAsString(profiles));
                    resp.setHeader("current", objectMapper.writeValueAsString(profile));
                }
                resp.setStatus(HttpServletResponse.SC_OK);
            }
        }
        logger.info("GET request successfully resolved for ProfileServlet.");
    }

    /**
     * Delete the profile indicated in the request if the token has the proper authority.
     * 200 -- profile deleted
     * 400 -- token invalid
     * 404 -- profile not found
     * 405 -- request does not have sufficient access level
     * 500 -- exception
     * 503 -- unable to successfully delete requested profile
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("DELETE request received by ProfileServlet.");

        // Validate token
        String token = req.getHeader("Authorization");
        HashMap<String, Object> keys = SecurityServiceUtil.validateToken(token);

        // Get body data string
        BufferedReader bufRead = req.getReader();
        String[] profileId = bufRead.readLine().split("=");

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (keys == null || keys.isEmpty() || profileId.length != 2) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {

            Profile profile = profileService.getProfileById((Integer) keys.get("id"));
            if (profile == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            } else {
                if (profile.getAccessLevel() != AccessLevel.MANAGER) {
                    resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                } else {
                    if (profileService.deleteProfileById(Integer.valueOf(profileId[1])) == 1) {
                        resp.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        resp.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                    }
                }
            }
        }
        logger.info("DELETE request successfully resolved for ProfileServlet.");
    }
}
