package dev.pete.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.pete.models.*;
import dev.pete.services.FormService;
import dev.pete.services.FormServiceImpl;
import dev.pete.services.ProfileService;
import dev.pete.services.ProfileServiceImpl;
import dev.pete.utilities.SecurityServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class FormServlet extends HttpServlet {
    private Logger logger = LogManager.getLogger(FormServlet.class);
    private ProfileService profileService = new ProfileServiceImpl();
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Get all the forms in the database that the token from the request authorizes the user to view.
     * 200 -- list of forms in response body
     * 400 -- token not valid
     * 404 -- could not find profile
     * 500 -- exception
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET request received by FormServlet.");
        // Validate token
        String token = req.getHeader("Authorization");
        HashMap<String, Object> keys = SecurityServiceUtil.validateToken(token);

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (keys == null || keys.isEmpty()) { // Incorrect/NonExistent token
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            Profile profile = profileService.getProfileById((Integer) keys.get("id"));
            if (profile == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            } else {
                // Attempt to create a form service instance
                try (FormService formService = new FormServiceImpl(profile)) {
                    List<Form> formList = formService.getAllForms();
                    String forms = objectMapper.writeValueAsString(formList);
                    resp.getWriter().write(forms);
                    resp.setStatus(HttpServletResponse.SC_OK);
                } catch (Exception e) { // Unable to get instance
                    logger.error(e.getMessage());
                }
            }
        }
        logger.info("GET request successfully resolved for FormServlet");
    }

    /**
     * Insert a new form based on the authorization and status given in the request.
     * 200 -- new form successfully created and added, returned in response body
     * 400 -- token invalid
     * 404 -- could not find the profile
     * 500 -- exception
     * 503 -- could not add form
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("PUT request received by FormServlet.");
        // Validate token
        String token = req.getHeader("Authorization");
        HashMap<String, Object> keys = SecurityServiceUtil.validateToken(token);

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (keys == null || keys.isEmpty()) { // Incorrect/Nonexistent token
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            Profile profile = profileService.getProfileById((Integer) keys.get("id"));
            if (profile == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                logger.error("Could not find the profile associated with id: " + keys.get("id"));
            } else {
                Integer employee = profile.getId();
                String input = req.getReader().readLine();
                BigDecimal amount = new BigDecimal(String.format("%.2f", Double.valueOf(input)));
                // Create form and retrieve formService instance
                try (FormService formService = new FormServiceImpl(profile)) {
                    Form form = new Form();
                    form.setId(SecurityServiceUtil.getId());
                    form.setFileDate(Timestamp.valueOf(LocalDateTime.now()));
                    form.setEmployee(employee);
                    form.setAmount(amount);
                    form.setStatus(FormStatus.PENDING);

                    if (formService.insertForm(form) == 1) {
                        resp.getWriter().write(objectMapper.writeValueAsString(form));
                        resp.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        logger.error("Unable to insert form: " + form);
                        resp.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                    }
                } catch (Exception e) { // Unable to get instance or error inserting form
                    logger.error(e.getMessage());
                }
            }
        }
        logger.info("PUT request successfully resolved for FormServlet.");
    }

    /**
     * Update the form from the request to the requested state.
     * 200 -- form successfully updated, returned in response body
     * 400 -- invalid token
     * 401 -- profile not authorized to make changes
     * 404 -- could not find profile
     * 409 -- manager trying to approve their own request
     * 500 -- exception
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST request received by FormServlet.");
        // Validate token
        String token = req.getHeader("Authorization");
        HashMap<String, Object> keys = SecurityServiceUtil.validateToken(token);

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (keys == null || keys.isEmpty()) { // Incorrect/Nonexistent token
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            try {
                Profile profile = profileService.getProfileById((Integer) keys.get("id"));
                if (profile == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    if (profile.getAccessLevel() != AccessLevel.MANAGER) {
                        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    } else {
                        // Get Form data from request body
                        BufferedReader bufRead = req.getReader();
                        String jsonForm = bufRead.readLine();
                        Form form = objectMapper.readValue(jsonForm, Form.class);

                        if(Objects.equals(form.getEmployee(), profile.getId())) {
                            resp.setStatus(HttpServletResponse.SC_CONFLICT);
                        } else {
                            form.setResolveDate(Timestamp.valueOf(LocalDateTime.now()));
                            form.setManager(profile.getId());

                            try (FormService formService = new FormServiceImpl(profile)) {
                                formService.updateForm(form);
                                resp.getWriter().write(objectMapper.writeValueAsString(form));
                                resp.setStatus(HttpServletResponse.SC_OK);
                            } catch (Exception e) {
                                logger.error(e.getMessage());
                            }
                        }
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        logger.info("POST request successfully resolved for FormServlet.");
    }

    /**
     * Delete the form with id found in the request.
     * 200 -- form successfully deleted
     * 400 -- invalid token
     * 404 -- could not find profile
     * 500 -- exception
     * 503 -- could not delete form
     * @param req http request object
     * @param resp http response object
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("DELETE request received by FormServlet.");
        // Validate token
        String token = req.getHeader("Authorization");
        HashMap<String, Object> keys = SecurityServiceUtil.validateToken(token);

        // Get body data string
        BufferedReader bufRead = req.getReader();
        String formId = bufRead.readLine().split("=")[1];

        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if (keys == null || keys.isEmpty()) { // Incorrect/Nonexistent token
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            Profile profile = profileService.getProfileById((Integer) keys.get("id"));
            if (profile == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            } else {
                // Retrieve formService instance
                try (FormService formService = new FormServiceImpl(profile)) {
                    if (formService.deleteForm(Integer.valueOf(formId)) == 1) {
                        resp.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        resp.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                    }
                } catch (Exception e) { // Unable to get instance
                    logger.error(e.getMessage());
                }
            }
        }
        logger.info("DELETE request successfully resolved for FormServlet.");
    }
}
