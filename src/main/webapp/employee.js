const loginPage = '/EXRESEL/login.html';
const curPage = '/EXRESEL/employee.html';
const formServe = '/EXRESEL/form';
let forms;
let moneyFormat = new Intl.NumberFormat('en-US', {
   style: 'currency',
   currency: 'USD'
 });

document.getElementById('create-form').addEventListener('click', addForm);
document.getElementById('out').addEventListener('click', function(){
   sessionStorage.clear();
   window.location.href = loginPage;
});

window.onload = async function() {
   let requestOptions = {
   method: 'GET',
   headers: {'Authorization' : sessionStorage.getItem('token')}
   };    

   forms = await fetch(formServe, requestOptions)
   .then(request => {
      if(request.status == 200) {
         return request.json();
      } else if(request.status == 400) {
         alert('You do not have proper authorization for this page. Please sign in again.');
         window.location.href = loginPage;
      }
   })
   .catch(error => alert('error', error));

   refreshTable();
}

async function addForm() {
   const amount = document.getElementById('amount').value;

   if (amount <= 0) {
      alert('Whoops! The requested amount must be at least 0.01 cents.');
      document.getElementById('amount').value = 0.01;
      return;
   }

   let requestOptions = {
      method: 'PUT',
      headers: {'Content-Type' : 'application/x-www-form-urlencoded',
               'Authorization' : sessionStorage.getItem('token')},
      body: amount
   };

   fetch(formServe, requestOptions)
   .then(response => {if(response.status == 200) displayTable()})
   .catch(error => alert(error));
}

function refreshTable() {
   let body = document.getElementById('form-info-body');
   while (body.firstChild) {
      body.remove(body.firstChild);
   }

   for(let form of forms) {
   let row = document.createElement('tr');
   let date1 = new Date(form.fileDate).toLocaleDateString();
   let money = moneyFormat.format(form.amount);
   row.innerHTML = `<td>${form.id}</td><td>${date1}</td><td>${money}</td><td>${form.status}</td>`
   body.appendChild(row);
   }
}