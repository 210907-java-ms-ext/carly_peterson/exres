const loginPg = '#';
const employeePg = '/EXRESEL/employee.html';
const managerPg = '/EXRESEL/manager.html';
const profServe = '/EXRESEL/profile';

if (document.getElementById('login-submit')) {
    document.getElementById('login-submit').addEventListener('click', attemptLogin);
}

if (document.getElementById('sign-up-submit')) {
    document.getElementById('sign-up-submit').addEventListener('click', attemptSignUp);
}

async function attemptLogin() {
    const username = document.getElementById('login-username').value;
    const password = document.getElementById('login-password').value;

    let urlencoded = new URLSearchParams(`username=${username}&password=${password}`);

    let requestOptions = {
    method: 'POST',
    headers: {'Content-Type':'application/x-www-form-urlencoded'},
    body: urlencoded
    };

    fetch(profServe, requestOptions)
    .then(response => {
        if (response.status == 200) {
            sessionStorage.setItem('token', response.headers.get('Authorization'));
            return response.text();
        } else {
            alert('Incorrect Username and Password', 'The credentials entered were incorrect. Please try again.');
        }
    })
    .then(redirect => {
        if (redirect != undefined)
            window.location.href = redirect;
    })
    .catch(error => alert(error));
}

async function attemptSignUp() {
    const username = document.getElementById('sign-up-username').value;
    const initPass = document.getElementById('init-password').value;
    const confirmedPass = document.getElementById('confirm-password').value;
    const firstName = document.getElementById('first-name').value;
    const lastName = document.getElementById('last-name').value;

    if (!username || !initPass || !firstName || !lastName) {
        alert('All sign up values must be filled!');
        return;
    }

    if (initPass !== confirmedPass) {
        alert('Passwords do not match. Cannot proceed.');
        window.location.href = loginPg;
    }

    const userData = `${username}\n${initPass}\n${firstName}\n${lastName}`;

    let requestOptions = {
    method: 'PUT',
    body: userData
    };

    fetch(profServe, requestOptions)
    .then(response => {
        if(response.status == 409) {
            alert('Account with that username already exists! Please choose a new one.');
            document.getElementById("sign-up-username").value = "";
        } else if (response.status == 200) {
            sessionStorage.setItem('token', response.headers.get('Authorization'));
            window.location.href = employeePg;
        }
    })
    .catch(error => alert(error));
}