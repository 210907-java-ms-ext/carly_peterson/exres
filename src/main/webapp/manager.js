const loginPage = '/EXRESEL/login.html';
const curPage = '/EXRESEL/manger.html';
const formServe = '/EXRESEL/form';
const profServe = '/EXRESEL/profile';
let forms; let employees;

let moneyFormat = new Intl.NumberFormat('en-US', {
   style: 'currency',
   currency: 'USD'
 });

document.getElementById('create-form').addEventListener('click', addForm);
document.getElementById('out').addEventListener('click', function(){
    sessionStorage.clear();
    window.location.href = loginPage;
});
document.getElementById('approve-btn').addEventListener('click', approveForm);
document.getElementById('reject-btn').addEventListener('click', rejectForm);

window.onload = function(){
   getForms(); getEmployees();
}

async function getForms() {
   let requestOptions = {
   method: 'GET',
   headers: {'Authorization' : sessionStorage.getItem('token')}
   };    

   forms = await fetch(formServe, requestOptions)
   .then(request => {
      if(request.status == 200) {
         return request.json();
      } else if(request.status == 400) {
         window.location.href = loginPage;
      }
   })
   .catch(error => alert(error));

   displayForms();
}

async function getEmployees() {
   let requestOptions = {
      method: 'GET',
      headers: {'Authorization' : sessionStorage.getItem('token'),
                  'current' : ''}
   };    
   
   employees = await fetch(profServe, requestOptions)
   .then(response => {
         if(response.status == 200) {
            let profile = JSON.parse(response.headers.get('current'));
            let head = document.getElementById('header');
            head.innerHTML += profile.firstName + ' ' + profile.lastName;
            return response.json();
         } else if(response.status == 400) {
            alert('You do not have proper authorization for this page. Please sign in again.');
            window.location.href = loginPage;
         }
   })
   .catch(error => alert(error));
}

function addForm() {
   const amount = document.getElementById('amount').value;

   if (amount <= 0) {
      alert('Whoops! The requested amount must be at least 0.01 cents.');
      document.getElementById('amount').value = 0.01;
      return;
   }

   let requestOptions = {
      method: 'PUT',
      headers: {'Content-Type' : 'application/x-www-form-urlencoded',
               'Authorization' : sessionStorage.getItem('token')},
      body: amount
   };

   fetch(formServe, requestOptions)
   .then(response => {if(response.status == 200) window.location.reload();})
   .catch(error => alert(error));
}

function displayForms() {
   let body = document.getElementById('form-info-rows');
   for(let form of forms) {
      let row = document.createElement('tr');
      let fileDate = new Date(form.fileDate).toLocaleDateString();

      row.innerHTML = `<td>${form.id}</td><td>${form.employee}</td>` +
                     `<td>${fileDate}</td><td>${moneyFormat.format(form.amount)}</td>` +
                     `<td>${form.status}</td>`;

      row.addEventListener('click', showDetails);

      body.appendChild(row);
   }
}

function clearForms() {
   let body = document.getElementById('form-info-rows');
   while(body.firstChild) {
      body.removeChild(body.firstChild);
   }
}

function clearDetails() {
   let editPanel = document.getElementById('edit-panel');
   let detailView = document.getElementById('detail-view');
   while(detailView.firstChild) {
      detailView.removeChild(detailView.firstChild);
   }
   return editPanel;
}

function showDetails(evt) {
   let editPanel = clearDetails();
   const fid = evt.currentTarget.firstElementChild.innerHTML;
   const eid = evt.currentTarget.firstElementChild.nextElementSibling.innerHTML;
   let form = getForm(fid);
   let employee = getEmployee(eid);
   let manager = getEmployee(form.manager);

   let detailView = document.getElementById('detail-view');
   let formId = document.createElement('p');
   formId.innerHTML = `<strong>Form Number: </strong>${form.id}`;

   let empName = document.createElement('p');
   empName.innerHTML = `<strong>Employee: </strong>${employee.firstName} ${employee.lastName}`;

   let dateFiled = document.createElement('p');
   let date1 = new Date(form.fileDate).toLocaleDateString();
   dateFiled.innerHTML = `<strong>Date Submitted: </strong>${date1}`;

   let status = document.createElement('p');
   status.innerHTML = `<strong>Status: </strong>${form.status}`;

   detailView.appendChild(formId);
   detailView.appendChild(empName);
   detailView.appendChild(dateFiled);
   detailView.appendChild(status);
   if (form.status == 'APPROVED' || form.status == 'REJECTED') {
      let manInfo = document.createElement('p');
      manInfo.innerHTML = `<strong>Resolved by Manager: </strong>${manager.firstName} ${manager.lastName}`
      let endDate = document.createElement('p');
      let date2 = new Date(form.resolveDate).toLocaleDateString();
      endDate.innerHTML = `<strong>Resolved on: </strong>${date2}`;

      detailView.appendChild(manInfo);
      detailView.appendChild(endDate);
   } else {
      editPanel.style = 'display: block';
   }
   detailView.appendChild(editPanel);
   detailView.style = 'display= block';
}

function getEmployee(id) {
   return employees.find(e => e.id == id);
}

function getForm(id) {
   return forms.find(f => f.id == id);
}

async function approveForm() {
   let fid = document.getElementById('detail-view').firstChild.lastChild.textContent;
   let form = getForm(fid);
   form.status = 'APPROVED';
   updateForm(form);
}

async function rejectForm() {
   let fid = document.getElementById('detail-view').firstChild.lastChild.textContent;
   let form = getForm(fid);
   form.status = 'REJECTED';
   updateForm(form);
}

function updateForm(form) {
   let requestOptions = {
      method: 'POST',
      headers: {'Authorization' : sessionStorage.getItem('token')},
      body: JSON.stringify(form)
   }

   fetch(formServe, requestOptions)
   .then(request => {
      if (request.status == 200) {
         return request.json();
      } else if (request.status == 409) {
         alert('Nice try, but you can\'t approve/reject your own requests.');
         return null;
      }
   })
   .then(jsonForm => {
      if (jsonForm != null) {
         form.manager = jsonForm.manager;
         form.resolveDate = jsonForm.resolveDate;
         clearForms();
         displayForms();
         document.getElementById('detail-view').style='display: none';
         document.getElementById('edit-panel').style='display: none';
      }
   })
   .catch(error => alert(error));
}