create table profile (
	e_id int primary key,
	username varchar(50) unique not null,
	pass_key varchar(772) not null,
	access_level varchar(8) not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null
);

create table reimbursement (
	r_id int primary key,
	employee int not null references profile(e_id),
	file_date timestamp not null,
	amount numeric(38, 2) not null,
	status varchar(20) not null,
	resolve_date timestamp,
	manager int references profile(e_id)
);