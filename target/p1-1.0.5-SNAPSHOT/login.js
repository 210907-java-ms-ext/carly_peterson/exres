/** 
 * EventListeners:
 * - button to sign in
 * - box to enter username
 * -> send to next box
 * - box to enter password
 * -> send sign in request
 * 
 * HTTP Requests:
 * - POST
 * -> send the username and password, recieve token
 * -> load correct profile page
*/
document.getElementById("login-submit").addEventListener("click", attemptLogin);
document.getElementById("sign-in-submit").addEventListener("click", attemptSignUp);

function attemptLogin() {
    const username = document.getElementById("login-username").value;
    const password = document.getElementById("login-password").value;

    let urlencoded = new URLSearchParams(`username=${username}&password=${password}`);

    let requestOptions = {
    method: 'POST',
    headers: {'Content-Type':'application/x-www-form-urlencoded'},
    body: urlencoded
    };

    fetch("http://localhost:8080/p1/login", requestOptions)
    .then(response => {
        if (response.status == 200) {
            sessionStorage.setItem("token", response.headers.get("Authorization"));
            return response.text();
        } else {
            alert("Incorrect Username and Password", "The credentials entered were incorrect. Please try again.");
        }
    })
    .then(redirect => {
        if (redirect != undefined)
            window.location.href = redirect;
    })
    .catch(error => alert('error', error));
}

function attemptSignUp() {
    const username = document.getElementById("sign-up-username").value;
    const initPass = document.getElementById("init-password").value;
    const confirmedPass = document.getElementById("confirm-password").value;

    if (initPass !== confirmedPass) {
        alert("Passwords do not match. Cannot proceed.");
        window.location.href="http://localhost:8080/p1/login.html";
    }
}