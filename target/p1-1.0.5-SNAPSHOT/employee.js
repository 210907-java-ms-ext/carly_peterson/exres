/**
 * HTTP Requests
 * - Get
 * -> get forms for that employee
 * - Post
 * -> add new form
 */
document.getElementById("create-form").addEventListener('click', addForm);

window.onload = function() {
   let requestOptions = {
   method: 'GET',
   headers: {'Authorization' : sessionStorage.getItem("token")}
   };    

   let data = fetch("http://localhost:8080/p1/employee", requestOptions)
   .then(request => {
      if(request.ok) return request.json();
      if(request.status == 401) {
         alert("You do not have proper authorization for this page. Please sign in again.");
         window.location.href = "http://localhost:8080/p1/login.html";
      }
   })
   .catch(error => alert('error', error));

   let forms = JSON.parse(data);
   let table = document.getElementById("form-table");

   for(let form of forms) {
      let row = document.createElement("tr");
      row.innerHTML = `<td>${form.id}</td><td>${form.fileDate}</td><td>${form.status}</td>`
      table.appendChild(row);
   }
}

function addForm() {
   const amount = document.getElementById("amount");

   let urlencoded = new URLSearchParams("");
   urlencoded.append('amount', amount);

   let requestOptions = {
      method: 'POST',
      headers: {'Authorization' : sessionStorage.getItem("token"),
               'Content-Type' : 'application/x-www-form-urlencoded'},
      body: urlencoded
   }

   fetch("http://localhost:8080/p1/employee", requestOptions)
   .then(response => {
      if(response.ok) window.location.reload();
   })
   .catch(error => alert('error', error));
}